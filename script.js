$(document).ready(function() {
$("a.form-button__link").click(function () {
	var elementClick = $(this).attr("href")
	var destination = $(elementClick).offset().top;
	jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
	return false;
});
});

$(document).ready(function() {
  $('#form').submit(function(e) {
    e.preventDefault();
    var name = $('#name').val();
    var post = $('#post').val();
    var phone = $('#phone').val();
    var interesting = $('#interesting').val();
    $(".error").remove();
    if (name.length < 2) {
      $('#name').after('<span class="error">В этом поле должно быть минимум 2 символа</span>');
    }
    if (post.length< 5) {
      $('#post').after('<span class="error">В этом поле должно быть минимум 5 символов</span>');
    } else {
      var regEx = /^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9-]{1,63}.){1,125}[A-Z]{2,63}$/;
      var validEmail = regEx.test(post);
      if (!validEmail) {
        $('#post').after('<span class="error">Впишите правильную почту</span>');
      }
  	}
    if (phone.length < 7) {
      $('#phone').after('<span class="error">В этом поле должно быть минимум 7 цифр</span>');
    	} else if (!Number(phone)) {
    		$('#phone').after('<span class="error">Телефон должен состоять из цифр</span>');
    	}
    if (interesting.length < 10) {
      	$('#interesting').after('<span class="error">В этом поле должно быть минимум 10 символов</span>');
    }
  });
});